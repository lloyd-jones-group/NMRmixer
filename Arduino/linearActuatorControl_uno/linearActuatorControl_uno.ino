/*
  Linear Actuator Control

  This program drives a unipolar or bipolar stepper linear actuator.
  The motor is attached to digital pins 9 - 13 of the Arduino.

  When activated the motor should revolve a fixed distance in one
  direction, then the same distance in the other direction. A TTL
  trigger sets the motor to move the actuator up to the highest
  position and blocks further motion whilst the TTL trigger is
  active.

  Commands can be sent to Arduino using the USB serial port:

  INITIALISE        :   Reset arduino
  START             :   Starts the motor moving
  STOP              :   Stops the motor
  STATUS            :   Returns the current status (ON, OFF, HOLD, ERROR)
  GET_POS           :   Returns the postion of the actuator in mm
  SET_POS []        :   Set the postion of the actuator in mm [float]
  SET_SPEED []      :   Set motor maximum speed in mm/s [float]
  SET_ACCEL []      :   Set motor acceleration in mm/s^2 [float]
  SET_DISTANCE []   :   Set distance for motor to move in mm [float]
  MOVE_UP           :   Move to 'up' position set by distance parameter
  MOVE_DOWN         :   Move to home position
  MOVE_TO []        :   Manually set target position for motor in mm [float]
  HOLD_ON           :   Switch on the hold function
  HOLD_OFF          :   Switch off the hold function
  HOLD_TEST         :   Test hold function

  (c) Andrew Hall 2021
  a.m.r.hall@ed.ac.uk

  Created: 11/12/2020
  Updated: 29/07/2021
  Version 2.8

*/


#include <AccelStepper.h>

// Motor hardware parameters
const int stepsPerRevolution = 200; // number of steps per full revolution
const float stepSize = 0.0075;      // step size in mm (Note: this is in microsteps, there are 10 microsteps per full step for this motor)
const byte interruptPin = 2;        // pin on Arduino that is used for TTL interupt
const byte upperLimitPin = 3;       // pin on Arduino that is used for upper limit switch
const byte lowerLimitPin = 4;       // pin on Arduino that is used for lower limit switch
const byte disablePin = 8;          // pin on Arduino that is used for motor disable switch


// Variables that will change:
bool on = false;                    // module on/off state
bool hold = false;                  // hold state used to prevent motor moving during acquisition
bool test = false;                  // test state for hold
String errs;                        // string for holding error messages
float stepRate = 2000;              // speed in steps/s
float accel = 160000;               // acceleration in steps/s^2
float distance = 6000;              // distance in steps


// initialize the stepper library on pins 11 and 12:
AccelStepper myStepper(AccelStepper::DRIVER, 11, 12, true);


void setup() {
  // initialize the serial port:
  Serial.begin(115200);
  while (!Serial) ;
  Serial.println("Linear Actuator Controller");

  // set the maximum speed and acceleration
  myStepper.setMaxSpeed(stepRate);
  myStepper.setAcceleration(accel);

  // set up TTL interupt (active low logic)
  pinMode(interruptPin, INPUT_PULLUP);
  if(digitalRead(interruptPin) == false){
    errs = "ERROR: TTL input from spectrometer not detected";
  }

  // set up limit switches
  pinMode(upperLimitPin, INPUT_PULLUP);
  pinMode(lowerLimitPin, INPUT_PULLUP);

  // set up disable switch
  pinMode(disablePin, INPUT);
}



void loop() {

  // Check for serial input.
  // To avoid juddering, only runs if speed <6000 steps/s.
  if (myStepper.isRunning() == false || stepRate <6000) {
      READ_SERIAL();
  }

  // Check if motor is powered on
  if (digitalRead(disablePin) == true){
    
    // Check if hold is on
    if (digitalRead(interruptPin) == LOW && hold == false) {
      HOLD_ON();
    }
    if (digitalRead(interruptPin) == HIGH && test == false && hold == true) {
      HOLD_OFF();
    }
  
    if (myStepper.isRunning() == false) {
      // Check if target position reached and reverse direction
      if (hold == false && on == true) {
        if (myStepper.targetPosition() == 0)
          myStepper.moveTo(distance);
        else
          myStepper.moveTo(0);
      }
    }

    // Take a maximum of one step as required
    myStepper.run();

    // Check if motor limit reached
    CHECK_LIMITS();
  
  }
}




//////////////////////////////////////
// FUNCTIONS
//////////////////////////////////////


void START() {
  on = true;
  Serial.println("ON");
}


void STOP() {
  on = false;
  Serial.println("OFF");
}


void STATUS() {
  if (errs.length() > 0) {
    Serial.println(errs);
    errs = "";
  }
  else if (digitalRead(disablePin) == false)
    Serial.println("OFF");
  else if (hold == true)
    Serial.println("HOLD");
  else if (on == true || myStepper.isRunning() == true)
    Serial.println("ON");
  else
    Serial.println("STANDBY");
}


void GET_POS() {
  Serial.println(myStepper.currentPosition() * stepSize);
}


void MOVE_UP() {
  myStepper.moveTo(distance);
}


void MOVE_DOWN() {
  myStepper.moveTo(0);
}


void SET_POS(float value) {
  if (value <= -100 || value > 100)
    Serial.println("Invalid position");
  else
    myStepper.setCurrentPosition(value/stepSize);
}


void SET_SPEED(float value) {
  if (value <= 0 || value > 500)
    Serial.println("Invalid speed");
  else
    stepRate = value / stepSize;
  myStepper.setMaxSpeed(stepRate);
}


void SET_ACCEL(float value) {
  if (value <= 0)
    Serial.println("Invalid acceleration value");
  else
    accel = value / stepSize;
  myStepper.setAcceleration(accel);
}


void SET_DISTANCE(float value) {
  if (value <= 0 || value > 100)
    Serial.println("Invalid distance");
  else
    distance = value / stepSize;
}


void MOVE_TO(float value) {
  myStepper.moveTo(value / stepSize);
}


void HOLD_ON() {
  hold = true;
  Serial.println("HOLD ON");
  // Move the motor to highest position
  myStepper.moveTo(distance);
}


void HOLD_OFF() {
  hold = false;
  Serial.println("HOLD OFF");
}


void HOLD_TEST() {
  if (hold == false) {
    test = true;
    HOLD_ON();
  }
  else {
    test = false;
    HOLD_OFF();
  }
}


void CHECK_LIMITS() {
  // Check if limit stops reached.
  // If limit reached, reverse direction away from limit
  if (digitalRead(upperLimitPin) == LOW) {
    errs = "ERROR: Upper limit reached";
    myStepper.move(-1);
  }
  else if (digitalRead(lowerLimitPin) == LOW) {
    errs = "ERROR: Lower limit reached";
    myStepper.move(1);
  }
}


void READ_SERIAL() {
  while (Serial.available() > 0) {
    String line = Serial.readStringUntil('\n');

    // run the function requested
    if (line.indexOf("INITIALISE") >= 0)
      setup();
    if (line.indexOf("START") >= 0)
      START();
    else if (line.indexOf("STOP") >= 0)
      STOP();
    else if (line.indexOf("STATUS") >= 0)
      STATUS();
    else if (line.indexOf("GET_POS") >= 0)
      GET_POS();
    else if (line.indexOf("MOVE_UP") >= 0)
      MOVE_UP();
    else if (line.indexOf("MOVE_DOWN") >= 0)
      MOVE_DOWN();
    else if (line.indexOf("HOLD_ON") >= 0)
      HOLD_ON();
    else if (line.indexOf("HOLD_OFF") >= 0)
      HOLD_OFF();
    else if (line.indexOf("HOLD_TEST") >= 0)
      HOLD_TEST();
    else if (line.indexOf("SET_POS") >= 0)
      SET_POS(line.substring(7).toFloat());
    else if (line.indexOf("SET_SPEED") >= 0)
      SET_SPEED(line.substring(9).toFloat());
    else if (line.indexOf("SET_ACCEL") >= 0)
      SET_ACCEL(line.substring(9).toFloat());
    else if (line.indexOf("SET_DISTANCE") >= 0)
      SET_DISTANCE(line.substring(12).toFloat());
    else if (line.indexOf("MOVE_TO") >= 0)
      MOVE_TO(line.substring(7).toFloat());
  }
}
