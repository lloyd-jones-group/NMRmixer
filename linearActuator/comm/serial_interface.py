# -*- coding: utf-8 -*-
"""
@author: Andrew Hall
@version: 2.5
@created: 18/12/2020
@updated: 22/12/2021

Python script for sending serial commands to linear actuator

Commands can be sent to Arduino using the USB serial port:
  INITIALISE        :   Reset arduino
  START             :   Starts the motor moving
  STOP              :   Stops the motor
  STATUS            :   Returns the current status (ON, OFF, HOLD, ERROR)
  GET_POS           :   Returns the postion of the actuator in mm
  SET_POS []        :   Set the postion of the actuator in mm [float]
  SET_SPEED []      :   Set motor maximum speed in mm/s [float]
  SET_ACCEL []      :   Set motor acceleration in mm/s^2 [float]
  SET_DISTANCE []   :   Set distance for motor to move in mm [float]
  MOVE_UP           :   Move to 'up' position set by distance parameter
  MOVE_DOWN         :   Move to home position
  MOVE_TO []        :   Manually set target position for motor in mm [float]
  HOLD_ON           :   Switch on the hold function
  HOLD_OFF          :   Switch off the hold function
  HOLD_TEST         :   Test hold function
  TRIG_ENABLE []    :   Enable/disable hardware trigger [1 = hardware trigger, 0 = software trigger]


"""

import serial, time
import serial.tools.list_ports as list_ports

class arduino(object):
    def __init__(self, parent, default_port):        
        self.log=parent.log
        
        # Find the Arduino and start serial communication
        self.connected = False
        
        # First try the port that was used last time
        ports=[default_port]
        
        # Otherwise check all connected serial devices in turn
        for device in list_ports.comports():
                ports.append(device.device)

        # Test the ports in turn to see if the motor drive is connected
        for port in ports:
            self.test_port(port)
            if self.connected == True:
                break
            else:
                pass
        
        # If device is not detected on any port, raise an error
        if self.connected==False:
            raise AttributeError
              
    
    def send(self, command):
        '''
        Send a command to the Arduino. Read any response.

        Parameters
        ----------
        command : str
            The command to send to the Arduino.

        Returns
        -------
        reply : str
            The reply from the Arduino (if any).

        '''
        self.arduino.reset_input_buffer()
        command = str(command) + "\n"
        self.arduino.write(bytes(command.encode('utf-8')))
        reply = self.arduino.readline()
        if len(reply) > 0:
            reply = reply.decode('utf-8').strip('\n')
        else:
            reply = None
        return reply
    
    
    def test_port(self, port):
        '''
        Test to see if motor drive is connected to the port.

        Parameters
        ----------
        port : str
            The COM port to test.

        Returns
        -------
        None.

        '''
        self.port = port
        self.log.write("Attempting to open connection on " + self.port + "...\n")
        try:
            
            # Attempt to open serial connection to the device
            self.arduino = serial.Serial(port=self.port, baudrate=115200, timeout=0.1, write_timeout=0.1)
            time.sleep(2)  # Arduino takes a couple of seconds to wake up
            
            # Send initialise command and check if device gives expected response
            reply=self.send('INITIALISE')
            if 'Linear Actuator Controller' in reply:
                self.connected=True
            else:
                self.arduino.close() 
                
        except:
            self.log.write("No reply on port " + self.port + ".\n")

        
    def close(self):
        '''
        Close serial communication.

        Returns
        -------
        None.

        '''
        self.arduino.close()

