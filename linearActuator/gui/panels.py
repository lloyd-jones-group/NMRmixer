# -*- coding: utf-8 -*-
"""
@author: Andrew Hall
@version: 2.1
@created: 09/01/2021
@updated: 26/08/2021

Content panels for GUI

"""
import wx
import wx.adv
from gui import widgets


class Control_Panel(wx.Panel):
    """ Create control panel """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
                
        # Create the GridSizer to use with the Panel
        grid = wx.GridBagSizer(vgap=0, hgap=0)        
        
        # Set up the input fields and labels
        self.speed = wx.SpinCtrl(self, size=(75, -1), min=0, max=120, style=wx.TE_PROCESS_ENTER)
        self.distance = wx.SpinCtrl(self, size=(75, -1), min=0, max=100, style=wx.TE_PROCESS_ENTER)  
        
        
        # Add the widgets to the grid sizer to handle layout        
        grid.Add(wx.StaticText(self, label='Speed [mm/s]:'), pos=(0,0), flag = wx.EXPAND|wx.ALL, border = 5)
        grid.Add(self.speed, pos=(0,1), flag = wx.EXPAND|wx.ALL, border = 5) 
        grid.Add(wx.StaticText(self, label='Distance [mm]:'), pos=(1,0), flag = wx.EXPAND|wx.ALL, border = 5)
        grid.Add(self.distance, pos=(1,1), flag = wx.EXPAND|wx.ALL, border = 5)
        
               
        # Set the sizer on the panel
        self.SetSizerAndFit(grid) 

        
        
class Buttons(wx.Panel):
    """ Create buttons panel """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        
        # Create the GridSizer to use with the Panel
        grid = wx.GridBagSizer(vgap=0, hgap=0)     
        
        # Create buttons
        self.on_button = wx.Button(self, label='START', size=(150,50))
        self.on_button.SetBackgroundColour(wx.GREEN) 
        self.off_button = wx.Button(self, label='STOP', size=(150,50))
        self.off_button.SetBackgroundColour(wx.RED)
        self.downButton = wx.Button(self, label='Move Down', size=(75,50))
        self.upButton = wx.Button(self, label='Move Up', size=(75,50))
        
        # Add the widgets to the grid sizer to handle layout
        grid.Add(self.on_button, pos=(0,0), span=(1,2), flag = wx.EXPAND|wx.TOP|wx.BOTTOM, border = 5)
        grid.Add(self.off_button, pos=(1,0), span=(1,2), flag = wx.EXPAND|wx.TOP|wx.BOTTOM, border = 5)
        grid.Add(self.upButton, pos=(3,0), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(self.downButton, pos=(3,1), flag = wx.EXPAND|wx.ALL, border = 1)
               
        # Set the sizer on the panel
        self.SetSizer(grid) 
        


class Status_Panel(wx.Panel):
    """ Create status panel """
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        #self.SetBackgroundColour((255, 255, 0))
        
        # Create static box to contain widgets
        box = wx.StaticBox(self, wx.ID_ANY, label="Status")
        bsizer = wx.StaticBoxSizer(box, wx.VERTICAL)
        
        # Create the GridSizer to use with the Panel
        grid = wx.GridBagSizer(vgap=1, hgap=1) 
        
        # Connection indicator
        connect_label = wx.StaticText(box, label='Connected:', style=wx.ALIGN_LEFT)
        self.connect_led = widgets.LED(self, color='g')

        # Status indicator
        status_label = wx.StaticText(box, label='Drive [On/Standby/Off]:', style=wx.ALIGN_LEFT)
        self.on_led = widgets.LED(box, color='g')
        self.standby_led = widgets.LED(box, color='y')
        self.off_led = widgets.LED(box, color='r')
        
        
        # Limit indicator
        limit_label = wx.StaticText(box, label='Limit [Upper/Lower]:', style=wx.ALIGN_LEFT)
        self.max_led = widgets.LED(box, color='r')
        self.min_led = widgets.LED(box, color='r')
        
        # Hold indicator
        hold_label = wx.StaticText(box, label='Hold:', style=wx.ALIGN_LEFT)
        self.hold_led = widgets.LED(box, color='y')
        
        # Add the widgets to the grid sizer to handle layout   
        grid.Add(connect_label, (0,0), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(self.connect_led.led, (0,1), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(status_label, (1,0), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(self.on_led.led, (1,1), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(self.standby_led.led, (1,2), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(self.off_led.led, (1,3), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(limit_label, (2,0), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(self.max_led.led, (2,1), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(self.min_led.led, (2,2), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(hold_label, (3,0), flag = wx.EXPAND|wx.ALL, border = 1)
        grid.Add(self.hold_led.led, (3,1), flag = wx.EXPAND|wx.ALL, border = 1)
        
        # Set main box sizer
        bsizer.Add(grid)
        self.SetSizer(bsizer)
        
        

class Log_Panel(wx.Panel):
    """Create log panel"""
    def __init__(self, parent):
        wx.Panel.__init__(self, parent, size=(300,150))
        
        log_label = wx.StaticText(self, label='Log:', style=wx.ALIGN_LEFT)
        self.log = wx.TextCtrl(self, style=wx.TE_MULTILINE | wx.TE_READONLY, size=(50, 20))

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(log_label, 0, wx.EXPAND)
        sizer.Add(self.log, 1, wx.EXPAND)
        self.SetSizer(sizer)