# -*- coding: utf-8 -*-
"""
@author: Andrew Hall
@version: 1.1
@created: 09/01/2021
@updated: 11/01/2021

Custom widgets

"""
import wx
import sys
import os


class LED(wx.Control):
    def __init__(self, parent, color, state=0):
        '''
        An LED indicator object for use in graphical user interfaces

        Parameters
        ----------
        parent : class
            Parent class for object
        color : str
            Color for LED object. 'r', 'g', 'y' or 'b'
        state : int
            State of LED object. 0=off, 1=on

        Returns
        -------
        LED object

        '''
        super().__init__()
    
        self.color = color
        self.state = state
        
        # Get path for files
        path = resource_path("icons")
        
        # Set styles for indicator LEDs
        self.led_g_on = wx.Bitmap(wx.Image(path + "\lamp-green-on.png").Scale(20,20))
        self.led_g_off = wx.Bitmap(wx.Image(path + "\lamp-green-off.png").Scale(20,20))
        self.led_r_on = wx.Bitmap(wx.Image(path + "\lamp-red-on.png").Scale(20,20))
        self.led_r_off = wx.Bitmap(wx.Image(path + "\lamp-red-off.png").Scale(20,20))
        self.led_y_on = wx.Bitmap(wx.Image(path + "\lamp-yellow-on.png").Scale(20,20))
        self.led_y_off = wx.Bitmap(wx.Image(path + "\lamp-yellow-off.png").Scale(20,20))
        self.led_b_on = wx.Bitmap(wx.Image(path + "\lamp-blue-on.png").Scale(20,20))
        self.led_b_off = wx.Bitmap(wx.Image(path + "\lamp-blue-off.png").Scale(20,20))
        
        if (color=='g' and state==0):
            self.led = wx.StaticBitmap(parent, bitmap=self.led_g_off)
        elif (color=='g' and state==1):
            self.led = wx.StaticBitmap(parent, bitmap=self.led_g_on)
        elif (color=='r' and state==0):
            self.led = wx.StaticBitmap(parent, bitmap=self.led_r_off)
        elif (color=='r' and state==1):
            self.led = wx.StaticBitmap(parent, bitmap=self.led_r_on)
        elif (color=='y' and state==0):
            self.led = wx.StaticBitmap(parent, bitmap=self.led_y_off)
        elif (color=='y' and state==1):
            self.led = wx.StaticBitmap(parent, bitmap=self.led_y_on)
        elif (color=='b' and state==0):
            self.led = wx.StaticBitmap(parent, bitmap=self.led_b_off)
        elif (color=='b' and state==1):
            self.led = wx.StaticBitmap(parent, bitmap=self.led_b_on)
        
            
    def update(self, state):
        '''
        

        Parameters
        ----------
        state : int
            State of LED object. 0=off, 1=on

        Returns
        -------
        None.

        '''
        if (self.color=='g' and state==0):
            self.led.SetBitmap(self.led_g_off)
        elif (self.color=='g' and state==1):
            self.led.SetBitmap(self.led_g_on)
        elif (self.color=='r' and state==0):
            self.led.SetBitmap(self.led_r_off)
        elif (self.color=='r' and state==1):
            self.led.SetBitmap(self.led_r_on)
        elif (self.color=='y' and state==0):
            self.led.SetBitmap(self.led_y_off)
        elif (self.color=='y' and state==1):
            self.led.SetBitmap(self.led_y_on)
        elif (self.color=='b' and state==0):
            self.led.SetBitmap(self.led_b_off)
        elif (self.color=='b' and state==1):
            self.led.SetBitmap(self.led_b_on)
        
 
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)

  