# -*- coding: utf-8 -*-
"""
@author: Andrew Hall
@version: 2.7
@created: 09/01/2021
@updated: 07/09/2022

Dialog boxes for GUI

"""
import wx
import wx.adv
import wx.lib.agw.floatspin as fs
        
        
        
class moveDialog(wx.Dialog):
    """ Create dialog window for setting position """
    def __init__(self,parent,cur_pos):
        super(moveDialog, self).__init__(parent, title ='Move to position')
        
        self.parent=parent
        self.functions = parent.functions
        
        self.InitUI(cur_pos)
        
        
        
    def InitUI(self, cur_pos):
        # Create panel and main sizer
        panel = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        
        # Create sizer for number input
        vbox = wx.BoxSizer(wx.VERTICAL)
        
        # Create numerical input
        label = wx.StaticText(panel, label='Current position [mm]:', style=wx.ALIGN_LEFT)
        #self.position = wx.SpinCtrlDouble(panel, initial=cur_pos, min=-150, max=150, style=wx.TE_PROCESS_ENTER)
        self.position = fs.FloatSpin(panel, -1, pos=(50, 50), min_val=-150, max_val=150,
                                 increment=0.5, value=cur_pos, agwStyle=fs.FS_LEFT)
        self.position.SetDigits(1)
        
        # Bind event for when number is changed
        self.position.Bind(fs.EVT_FLOATSPIN, self.functions.moveTo)
        
        # Add to sizer
        vbox.Add(label, flag=wx.ALL|wx.EXPAND, border=5)
        vbox.Add(self.position, flag=wx.ALL|wx.EXPAND, border=5)
        panel.SetSizer(vbox)

        # Add buttons
        hbox = wx.BoxSizer(wx.HORIZONTAL)
        setHomeButton = wx.Button(self, label='Set home position')
        setHomeButton.Bind(wx.EVT_BUTTON, self.setHome)
        okButton = wx.Button(self, id=wx.ID_OK, label='OK')
        closeButton = wx.Button(self, id=wx.ID_CANCEL, label='Close')
        
        # Add buttons to button sizer
        hbox.Add(setHomeButton, flag=wx.ALL, border=5)
        hbox.Add(okButton, flag=wx.ALL, border=5)
        hbox.Add(closeButton, flag=wx.ALL, border=5)

        # Add widgets to main sizer
        sizer.Add(panel, proportion=1, flag=wx.ALL|wx.EXPAND, border=5)
        sizer.Add(hbox, flag=wx.ALIGN_CENTRE|wx.TOP|wx.BOTTOM, border=10)

        # Set sizer for dialog window
        self.SetSizerAndFit(sizer)

        
    def setHome(self, event):
        """
        Set current position to zero and call setHome function

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.position.SetValue(0.0)
        self.functions.setHome(event)



class holdDialog(wx.Dialog): 
    """ Create dialog for hold settings """
    def __init__(self, parent): 
        super(holdDialog, self).__init__(parent, title = 'Hold settings') 
        
        self.parent = parent  
        self.functions = parent.functions
        self.parameters = parent.functions.parameters
        self.InitUI() 
           
       
    def InitUI(self):    
        panel = wx.Panel(self)
        
        # Create a sizer for the window
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # Create a sizer for the content
        grid = wx.GridBagSizer(vgap=5, hgap=1)
            
        #Create radio box
        lblList = ['Disabled', 'Hardware triggered', 'Software triggered'] 
		  
        self.rbox = wx.RadioBox(panel, label = 'Hold mode:', pos = (80,10), choices = lblList,
                              majorDimension = 0, style = wx.RA_SPECIFY_ROWS) 
        self.rbox.SetSelection(self.parameters['Trigger mode'])
        self.rbox.Bind(wx.EVT_RADIOBOX,self.onRadioBox) 

        # Create numerical input
        self.label1 = wx.StaticText(panel, label='Mixing time (s):', style=wx.ALIGN_LEFT)
        self.trigInterval = fs.FloatSpin(panel, -1, pos=(50, 50), min_val=1,
                                         increment=1, value=self.parameters['Trigger interval'],
                                         agwStyle=fs.FS_LEFT)
        self.trigInterval.SetDigits(0)
        self.label2 = wx.StaticText(panel, label='Trigger duration (s):', style=wx.ALIGN_LEFT)
        self.trigDuration = fs.FloatSpin(panel, -1, pos=(50, 50), min_val=1,
                                 increment=1, value=self.parameters['Trigger duration'], 
                                 agwStyle=fs.FS_LEFT)
        self.trigDuration.SetDigits(0)
        self.label3 = wx.StaticText(panel, label='Post-trigger delay (s):', style=wx.ALIGN_LEFT)
        self.trigDelay = fs.FloatSpin(panel, -1, pos=(50, 50), min_val=0,
                                 increment=1, value=self.parameters['Trigger delay'], 
                                 agwStyle=fs.FS_LEFT)
        self.trigDelay.SetDigits(0)
        self.numericalInput = [self.label1, 
                               self.label2, 
                               self.label3,
                               self.trigInterval, 
                               self.trigDuration,
                               self.trigDelay]
        
        if self.parameters['Trigger mode'] != 2:
            for widget in self.numericalInput:
                widget.Disable()    

        # Add buttons
        okButton = wx.Button(panel, id=wx.ID_OK, label='OK')
        testButton = wx.Button(panel, id=wx.ID_ANY, label='Test hold')
        testButton.Bind(wx.EVT_BUTTON, self.functions.testHold)
        cancelButton = wx.Button(panel, id=wx.ID_CANCEL, label='Cancel')
        
        # Add widgets to main sizer
        grid.Add(self.rbox, (0,0), span=(1,3), flag=wx.ALL|wx.EXPAND, border=10)
        grid.Add(self.label1, (1,0), flag=wx.ALL, border=5)
        grid.Add(self.trigInterval, (1,1), flag=wx.ALL, border=5)
        grid.Add(self.label2, (2,0), flag=wx.ALL, border=5)
        grid.Add(self.trigDuration, (2,1), flag=wx.ALL, border=5)
        grid.Add(self.label3, (3,0), flag=wx.ALL, border=5)
        grid.Add(self.trigDelay, (3,1), flag=wx.ALL, border=5)
        grid.Add(okButton, (4,0), flag=wx.ALL, border=10)
        grid.Add(testButton, (4,1), flag=wx.ALL, border=10)
        grid.Add(cancelButton, (4,2), flag=wx.ALL, border=10)

        # Set sizer for dialog window
        panel.SetSizer(grid)
        main_sizer.Add(panel)
        self.SetSizerAndFit(main_sizer)
        
        
    def onRadioBox(self, event): 
        if self.rbox.GetSelection() == 2:
            for widget in self.numericalInput:
                widget.Enable()
        else:
            for widget in self.numericalInput:
                widget.Disable()
            
