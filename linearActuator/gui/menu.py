# -*- coding: utf-8 -*-
"""
@author: Andrew Hall
@version: 2.5
@created: 09/01/2021
@updated: 22/12/2021

Menu bar for GUI

"""
import wx
import wx.adv



class MenuBar(wx.MenuBar):
    """ Create menu bar """
    def __init__(self, parent):
        wx.Panel.__init__(self)

        # File menu
        fileMenu = wx.Menu()
        self.connectMenuItem = fileMenu.Append(wx.ID_ANY, "&Connect", "Connect to the system")
        self.exitMenuItem = fileMenu.Append(wx.ID_EXIT, "&Exit\tAlt-X", "Close window and exit program.")
        self.Append(fileMenu, "&File")
        
        # Advanced menu
        advancedMenu = wx.Menu()
        moveMenu = wx.Menu()
        self.accelMenuItem = advancedMenu.Append(wx.ID_ANY, "Set acceleration","Set motor acceleration")
        advancedMenu.AppendSubMenu(moveMenu, "Move") 
        self.positionMenuItem = advancedMenu.Append(wx.ID_ANY, "Get position","Get current motor position")
        self.homeMenuItem = advancedMenu.Append(wx.ID_ANY, "Set home position","Set motor home position")
        self.calibrateMenuItem = advancedMenu.Append(wx.ID_ANY, "Calibrate motor drive", "Calibrate motor position using limit switch")
        self.holdMenuItem = advancedMenu.Append(wx.ID_ANY, "Hold settings","Change hold mode and settings")
        self.Append(advancedMenu, "&Advanced")
        
        # Move sub-menu
        self.moveUpMenuItem = moveMenu.Append(wx.ID_ANY, "Move up","Move motor to position set by distance parameter")
        self.moveDownMenuItem = moveMenu.Append(wx.ID_ANY, "Move down","Move motor to home position")
        self.moveMenuItem = moveMenu.Append(wx.ID_ANY, "Move to position","Manually set target position")
        
        # Help menu
        helpMenu = wx.Menu()
        self.aboutMenuItem = helpMenu.Append(wx.ID_ANY, "&About\tF1", "Information about this program")
        self.Append(helpMenu, "&Help")