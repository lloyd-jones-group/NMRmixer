# -*- coding: utf-8 -*-
"""
@author: Andrew Hall
@version: 2.8
@created: 18/12/2020
@updated: 21/10/2022

GUI for controlling linear actuator

"""

import wx
import wx.adv
import functions
from gui import panels
from gui import menu
import sys
import os

version = 'v2.8'

class MainWindow(wx.Frame):
    """ Create main frame for GUI """
    def __init__(self, version):
        self.version = version
        wx.Frame.__init__(self, None, -1, title='NMR mixing control ' + self.version)
        
        # Set icon
        path = resource_path("gui\icons")
        self.SetIcon(wx.Icon(path + "\icon.ico"))
        
        # Set default parameters
        self.parameters = {'Speed' : 50, 
                           'Acceleration' : 1200, 
                           'Distance' : 40, 
                           'Sensor position' : 70.0,
                           'COM port' : 'COM1',
                           'Trigger mode' : 1,
                           'Trigger interval' : 60,
                           'Trigger duration' : 10,
                           'Trigger delay' : 5}

        # Create content
        self.initContent()
        self.Centre()
             
        # Import functions
        self.functions=functions.Functions(self)
        
        # Read starting parameters from file
        self.functions.readParams()
        
        # Create timers
        self.mainTimer = wx.Timer(self)
        self.holdTimer = wx.Timer(self)
        self.postTimer = wx.Timer(self)
        
        # Bind events
        self.bindEvents()
        
        
    def initContent(self):
        """ Add panels and content """
         # Create top level panel - fixes background colour issue
        panel = wx.Panel(self)
        
        # Create a sizer for the window
        main_sizer = wx.BoxSizer()
        
        # Create the menu bar
        self.menuBar = menu.MenuBar(self)
        self.SetMenuBar(self.menuBar)
    
        # Create a sizer for the content
        grid = wx.GridBagSizer(vgap=1, hgap=1)
        
        # Create the content panels 
        self.controls = panels.Control_Panel(panel)
        self.buttons = panels.Buttons(panel)
        self.status = panels.Status_Panel(panel)
        self.log = panels.Log_Panel(panel)
        
        # Connect button
        self.connect_button = wx.Button(panel, label='Connect to system', size=(150,50))
        
        # Initialise motor button
        self.calibrate_button = wx.Button(panel, label='Calibrate motor drive', size=(150,50))
        
        # Controls should be disabled until connected to device
        self.buttons.Disable()
        self.controls.Disable()
        self.calibrate_button.Disable()
        self.menuBar.EnableTop(1, False)
        
        # Add the panels to the sizer
        grid.Add(self.connect_button, (0,0), flag=wx.ALL, border=10)
        grid.Add(self.calibrate_button, (0,1), flag=wx.ALL, border=10)
        grid.Add(self.controls, (1,0), flag=wx.EXPAND | wx.ALL, border=10)
        grid.Add(self.buttons, (1,1), span=(2,1), flag=wx.EXPAND | wx.ALL, border=10)
        grid.Add(self.status, (2,0), flag=wx.EXPAND | wx.ALL, border=10)
        grid.Add(self.log, (3,0), span=(1,2), flag=wx.EXPAND | wx.ALL, border=10)
        
        # Set the sizer
        panel.SetSizer(grid)
        main_sizer.Add(panel)
        self.SetSizerAndFit(main_sizer)
        
   
    def bindEvents(self):
        """ Bind events """
        # Bind button press events
        self.buttons.on_button.Bind(wx.EVT_BUTTON, self.functions.switchOn)        
        self.buttons.off_button.Bind(wx.EVT_BUTTON, self.functions.switchOff)
        self.buttons.upButton.Bind(wx.EVT_BUTTON, self.functions.moveUp)
        self.buttons.downButton.Bind(wx.EVT_BUTTON, self.functions.moveDown)
        self.connect_button.Bind(wx.EVT_BUTTON, self.functions.connect)
        self.calibrate_button.Bind(wx.EVT_BUTTON, self.functions.calibrateMotor)
        
        # Bind numerical input events
        self.controls.speed.Bind(wx.EVT_TEXT_ENTER, self.functions.setSpeed)
        self.controls.speed.Bind(wx.EVT_SPINCTRL, self.functions.setSpeed)
        self.controls.distance.Bind(wx.EVT_TEXT_ENTER, self.functions.setDistance)
        self.controls.distance.Bind(wx.EVT_SPINCTRL, self.functions.setDistance)
        
        # Bind menu events
        self.menuBar.Bind(wx.EVT_MENU, self.functions.connect, self.menuBar.connectMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.OnClose, self.menuBar.exitMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.setAccel, self.menuBar.accelMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.getPosition, self.menuBar.positionMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.setHome, self.menuBar.homeMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.calibrateMotor, self.menuBar.calibrateMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.holdSettings, self.menuBar.holdMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.moveUp, self.menuBar.moveUpMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.moveDown, self.menuBar.moveDownMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.setPosition, self.menuBar.moveMenuItem)
        self.menuBar.Bind(wx.EVT_MENU, self.functions.OnAbout, self.menuBar.aboutMenuItem)
        
        # Bind timers
        self.Bind(wx.EVT_TIMER, self.functions.updateStatus, self.mainTimer)
        self.Bind(wx.EVT_TIMER, self.functions.updateHold, self.holdTimer)
        self.Bind(wx.EVT_TIMER, self.functions.setOutput, self.postTimer)
        
        # Bind window to close event
        self.Bind(wx.EVT_CLOSE, self.functions.OnClose)
        



class MainApp(wx.App):
    def OnInit(self):
        """ Initialise the GUI display"""
        '''
        # Splash screen
        path = resource_path("gui\icons")
        bitmap = wx.Bitmap(path + "\icon.png", wx.BITMAP_TYPE_PNG)
        splash = wx.adv.SplashScreen(bitmap, wx.adv.SPLASH_CENTRE_ON_PARENT | wx.adv.SPLASH_TIMEOUT,
                                     5000, None, -1, wx.DefaultPosition, wx.DefaultSize,
                                     wx.BORDER_SIMPLE | wx.STAY_ON_TOP)
        '''
        # Create main frame
        frame = MainWindow(version)
        frame.Show()
        # Indicate whether processing should continue or not
        return True
    
    
    def OnExit(self):
        """ Executes when the GUI application shuts down"""
        print('Exiting...')
        # Need to indicate success or failure
        return True




def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)




# Run the GUI application
app = MainApp()
app.MainLoop()
