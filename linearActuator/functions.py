# -*- coding: utf-8 -*-
"""
@author: Andrew Hall
@version: 2.0
@created: 09/01/2021
@updated: 28/10/2022

Functions for controlling linear actuator

"""
import wx
import wx.adv
from comm import serial_interface
from gui import dialog
import sys
import os
import json
import time



class Functions():     
    def __init__(self,parent):
        self.main = parent
        self.controls = parent.controls
        self.status = parent.status
        self.buttons = parent.buttons
        self.log = parent.log.log
        self.parameters = parent.parameters
    
    
    
    def readParams(self):
        """
        Read initial parameters from JSON file. 
        If the file is not found then the default parameters are used
        
        Returns
        -------
        None.
        
        """
        try:
            # reading the data from the file
            with open('setup.json') as f:
                data = f.read()
                  
            # reconstructing the data as a dictionary
            self.parameters = json.loads(data)
        
        except:
            self.log.write("Initial parameter file not found! Using default parameters.\n")

        # Set the starting values of the input fields
        self.controls.speed.SetValue(self.parameters['Speed'])
        self.controls.distance.SetValue(self.parameters['Distance'])
        
    
    
    def saveParams(self, parameters):
        """
        Write parameters to JSON file for future use.

        Parameters
        ----------
        parameters : dict
            A dictionary containing the parameters to be saved.
            
        Returns
        -------
        None.

        """
        # open file for writing, "w" 
        f = open("setup.json","w")
        
        # write dictionary to json file
        f.write(json.dumps(parameters, indent=1))
        
        # close file
        f.close()
    
    
    
    def connect(self,event):
        """
        Connect to arduino device over USB serial

        Returns
        -------
        None.

        """
        # Open connection to arduino controller
        self.log.write('Trying to connect...\n')
        try:
            self.arduino = serial_interface.arduino(self, self.parameters['COM port'])
            
        # If connection fails, open dialog with instructions for user
        except:
            self.log.write('Connection to drive unit failed!\n\n')
            wx.MessageBox('Connection to drive unit failed!\n'
                          'Check that USB cable is connected and try again.',
                          'Connection failed', wx.OK | wx.ICON_ERROR)
            
        # If connection is successful, print message and enable controls
        else:
            self.parameters['COM port'] = self.arduino.port
            self.log.write('Connected to drive unit on ' + self.parameters['COM port'] + '\n\n')
            self.main.connect_button.Disable()
            self.buttons.Enable()
            self.controls.Enable()
            self.main.calibrate_button.Enable()
            self.buttons.on_button.Enable()
            self.buttons.off_button.Disable()
            self.main.menuBar.EnableTop(1, True)
            self.main.menuBar.connectMenuItem.Enable(False)
            self.status.connect_led.update(state=1)
            self.on=False
            self.send('HOLD_OFF')
            self.main.mainTimer.Start(200)
            
            # Send parameters to the arduino
            self.send('SET_SPEED ' + str(self.parameters['Speed']))
            self.send('SET_ACCEL ' + str(self.parameters['Acceleration']))
            self.send('SET_DISTANCE ' + str(self.parameters['Distance']))
            self.setTriggerMode(self.parameters['Trigger mode'])
    
    
    
    def connectionError(self):
        """
        Connection error handler

        Returns
        -------
        None.

        """
        # If connection is lost, print message and disable controls
        self.log.write('Connection to drive unit lost!\n\n')
        self.arduino.connected=False
        self.main.connect_button.Enable()
        self.buttons.Disable()
        self.controls.Disable()
        self.main.calibrate_button.Disable()
        self.disableLEDs()
        self.main.menuBar.EnableTop(1, False)
        self.main.menuBar.connectMenuItem.Enable(True)
        self.status.connect_led.update(state=0)
        self.main.mainTimer.Stop()
        self.main.holdTimer.Stop()
        
        # Open dialog asking to try again
        errorBox = wx.MessageBox('Connection to drive unit lost!\n'
                      'Would you like to re-connect?',
                      'Connection failed', wx.OK | wx.CANCEL | wx.ICON_ERROR)
        if errorBox == wx.OK:
            self.connect(wx.OK) 
            
            
            
    def disableLEDs(self):
        """
        Set all LED indicators to 'off' state

        Returns
        -------
        None.

        """
        self.status.on_led.update(state=0)
        self.status.off_led.update(state=0)
        self.status.standby_led.update(state=0)
        self.status.hold_led.update(state=0)
        self.status.max_led.update(state=0)
        self.status.min_led.update(state=0)
     
            
     
    def send(self, command):
        """
        Send serial command and handle reply

        Parameters
        ----------
        command : str
            Command to device

        Returns
        -------
        reply : str
            Reply from device

        """
        try:
            reply = self.arduino.send(command)
        except:
            self.connectionError()
        else:
            return reply
        
        
        
    def calibrateMotor(self, event):
        """
        Calibrate the motor using the limit switches. The motor is moved until
        it reaches the upper limit switch. The motor position is then set to 
        the known position of the sensor.

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        # set the motor to low speed for safety
        self.send('SET_SPEED 10')
        
        # move the motor to an arbitarily high position so that it hits the upper limit sensor
        self.log.write('Calibration started: Moving to find the limit sensor...\n')
        self.send('MOVE_TO ' + '100')
        
        # wait for the motor to complete the movement
        while not ('Upper limit reached' in self.updateStatus(None)):
            time.sleep(0.2)
        
        # set the motor position to the known position of the sensor
        self.log.write('Limit sensor found at ' + str(self.parameters['Sensor position']) +' mm\n')
        self.send('SET_POS ' + str(self.parameters['Sensor position']))
        
        # set the speed back to normal
        self.send('SET_SPEED ' + str(self.parameters['Speed']))
        
        # move back to the home position
        self.log.write('Calibration complete. Moving to home position...\n\n')
        self.moveDown(None)

            
        
        
    def switchOn(self, event):
        """
        Switch device on

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.on=True
        self.setSpeed(event)
        self.setDistance(event)
        self.buttons.off_button.Enable()
        self.buttons.on_button.Disable()
        self.main.calibrate_button.Disable()
        self.main.menuBar.positionMenuItem.Enable(False)
        self.main.menuBar.homeMenuItem.Enable(False)
        self.main.menuBar.moveUpMenuItem.Enable(False)
        self.main.menuBar.moveDownMenuItem.Enable(False)
        self.main.menuBar.moveMenuItem.Enable(False)
        self.send('HOLD_OFF')
        self.send('START')
        self.log.write('Drive unit ON\n')
        
        
        
    def switchOff(self, event):
        """
        Switch device off

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.on=False
        self.buttons.on_button.Enable()
        self.buttons.off_button.Disable()
        self.main.calibrate_button.Enable()
        self.main.menuBar.positionMenuItem.Enable(True)
        self.main.menuBar.homeMenuItem.Enable(True)
        self.main.menuBar.moveUpMenuItem.Enable(True)
        self.main.menuBar.moveDownMenuItem.Enable(True)
        self.main.menuBar.moveMenuItem.Enable(True)
        self.send('STOP')
        self.send('HOLD_OFF')
        self.log.write('Drive unit OFF\n')

    
        
    def updateStatus(self, event):
        """
        Get status from device and update status panel. Called by wx.Timer

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        status : str
            The status reply from the Arudino

        """
        try:
            status=self.send('STATUS')
        except:
            self.connectionError()
        else:
            if status != None:
                if 'ON' in status:
                    self.hold=False
                    self.disableLEDs()
                    self.status.on_led.update(state=1)
                elif 'OFF' in status:#
                    self.hold=False
                    self.disableLEDs()
                    self.status.off_led.update(state=1)
                elif 'STANDBY' in status:
                    self.hold=False
                    self.disableLEDs()
                    self.status.standby_led.update(state=1)
                if 'HOLD' in status:
                    if self.parameters['Trigger mode']==2 and self.hold==False:
                        self.main.postTimer.StartOnce(1000*self.parameters['Trigger delay'])
                        self.main.holdTimer.StartOnce(1000*self.parameters['Trigger duration'])
                        self.hold=True
                    self.disableLEDs()
                    self.status.on_led.update(state=1)
                    self.status.hold_led.update(state=1)
                if 'ERROR' in status:
                    self.disableLEDs()
                    if self.on==True: 
                        self.status.on_led.update(state=1)
                    else:
                        self.status.off_led.update(state=1)
                    if 'Upper limit reached' in status:
                        self.status.max_led.update(state=1)
                    elif 'Lower limit reached' in status:
                        self.status.min_led.update(state=1)
                    else:
                        self.log.write(status + "\n")
                        self.errors = status
                return status
                    
            
    def setSpeed(self,event):
        """
        Get speed from GUI and send to device

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        speed = self.controls.speed.GetValue()
        self.parameters['Speed'] = speed
        self.send('SET_SPEED ' + str(speed))
        self.log.write('Set speed: ' + str(speed) + ' mm/s\n')
    
    
    
    def setDistance(self,event):
        """
        Get target distance from GUI and send to device

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        distance = self.controls.distance.GetValue()
        self.parameters['Distance'] = distance
        self.send('SET_DISTANCE ' + str(distance))
        self.log.write('Set distance: ' + str(distance) + ' mm\n')

             
   
    def setAccel(self,event):
        """
        Set the acceleration using a dialog box

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        accel = self.parameters['Acceleration']
        self.parameters['Acceleration'] = wx.GetNumberFromUser('Acceleration [mm/s\u00B2]:', '', 'Set acceleration', accel, min=1, max=5000)
        if self.parameters['Acceleration'] != -1:
            self.send('SET_ACCEL ' + str(self.parameters['Acceleration']))
            self.log.write('Set acceleration: ' + str(self.parameters['Acceleration']) + ' mm/s\u00B2\n')
        else:
            self.parameters['Acceleration'] = accel
        
        
        
    def moveUp(self,event):
        """
        Move the motor to the position set by the 'distance' parameter

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.send('HOLD_OFF')
        self.send('MOVE_UP')
                
            
            
    def moveDown(self,event):
        """
        Move the motor to the home position

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.send('HOLD_OFF')
        self.send('MOVE_DOWN')
        
        
        
    def setPosition(self,event):
        """
        Open a dialog that allows the user to fine tune the position.
        Position is automatically sent to the motor every time the value is 
        updated.

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        cur_pos = float(self.send('GET_POS'))
        self.dlg = dialog.moveDialog(self.main, cur_pos)
        if self.dlg.ShowModal() == wx.ID_OK:
            self.getPosition(event)
            
            
            
    def getPosition(self,event):
        """
        Request current position from motor and print to log.

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.log.write('Current position: ' + self.send('GET_POS').strip() + ' mm\n')


        
    def moveTo(self,event):
        """
        Send the target position to the motor.

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        position = self.dlg.position.GetValue()
        self.send('MOVE_TO ' + str(position))
        
        
        
    def setHome(self,event):
        """
        Set current motor position as home. 
        Calculate the new relative position of the upper limit sensor.

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        # Get the current position of the motor
        cur_pos = float(self.send('GET_POS'))
        
        # Calculate the relative position of the limit switch
        self.parameters['Sensor position'] -= cur_pos
        
        # Set the new position of the motor
        self.send('SET_POS 0.0')
        self.log.write('New home position set\n')
       
        
            
    def holdSettings(self,event):
        """
        Open a dialog that allows the user to set the mode used for hold.

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.dlg = dialog.holdDialog(self.main)
        if self.dlg.ShowModal() == wx.ID_OK:
            self.parameters['Trigger interval'] = self.dlg.trigInterval.GetValue()
            self.parameters['Trigger duration'] = self.dlg.trigDuration.GetValue()
            self.parameters['Trigger delay'] = self.dlg.trigDelay.GetValue()
            self.setTriggerMode(self.dlg.rbox.GetSelection())
            
            
            
    def setTriggerMode(self, mode):
        """
        Sets the trigger mode used for setting the hold.

        Parameters
        ----------
        mode : int
            Trigger mode:   0 = Disabled, 
                            1 = Hardware trigger, 
                            2 = Software trigger

        Returns
        -------
        None.

        """
        self.parameters['Trigger mode'] = mode
        if mode == 0:
            self.send('TRIG_ENABLE 0')
            self.log.write('Hold mode: Disabled\n')
        if mode == 1:
            self.send('TRIG_ENABLE 1')
            self.log.write('Hold mode: Hardware trigger\n')
        if mode == 2:
            self.send('TRIG_ENABLE 0')
            self.log.write('Hold mode: Software trigger\n')
            self.main.holdTimer.Start(1000*self.parameters['Trigger interval'])
        else:
            self.main.holdTimer.Stop()
            self.send('HOLD_OFF')
            
            
            
    def updateHold(self, event):
        """
        Timer event to update the hold in software trigger mode.

        Parameters
        ----------
        event : Timer event
            DESCRIPTION.

        Returns
        -------
        None.

        """
        if self.on == True:
            if self.main.holdTimer.IsOneShot():
                self.send('HOLD_OFF')
                self.main.holdTimer.Start(1000*self.parameters['Trigger interval'])
            else:
                self.send("HOLD_ON")
        
        
        
    def setOutput(self, event):
        """
        Timer event to set the TTL output in software trigger mode.
        Output is automatically cleared by Arduino when the motor is running.

        Parameters
        ----------
        event : Timer event
            DESCRIPTION.

        Returns
        -------
        None.

        """        
        self.send('SET_OUTPUT 1')
    
    
    
    def testHold(self,event):
        """
        Send a test signal to the motor to simulate a hold signal from the 
        spectrometer.

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.send('HOLD_TEST')
        dialog = wx.MessageDialog(None, #
                                  message="Hold signal active. Press 'OK' to end hold.",
                                          caption='Hold', style=wx.OK)
        dialog.ShowModal()
        self.send('HOLD_TEST')



    def OnAbout(self,event):
        """
        About software

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        info = wx.adv.AboutDialogInfo()

        description = 'User interface for controlling NMR mixing apparatus.'
        
        licence = """This program is free software; you can redistribute it 
                    and/or modify it under the terms of the GNU General Public 
                    License as published by the Free Software Foundation; 
                    either version 2 of the License, or (at your option) any 
                    later version.
                    
                    This program  is distributed in the hope that it will be 
                    useful, but WITHOUT ANY WARRANTY; without even the implied 
                    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR 
                    PURPOSE. See the GNU General Public License for more 
                    details. You should have received a copy of the GNU General 
                    Public License along with this program; if not, write to 
                    the Free Software Foundation, Inc., 59 Temple Place,
                    Suite 330, Boston, MA  02111-1307  USA"""
        
        info.SetName('NMR mixer control')
        info.SetIcon(wx.Icon(resource_path("gui\icons\icon.ico")))
        info.SetVersion(self.main.version)
        info.SetDescription(description)
        info.SetCopyright('(C) 2022 University of Edinburgh')
        info.SetWebSite('https://github.com/AMRHall/NMRstirring')
        info.SetLicence(licence)
        info.AddDeveloper('Andrew Hall')

        wx.adv.AboutBox(info)
        
        
        
    def OnClose(self, event):
        """
        Confirm close, shut down serial communication and exit.

        Parameters
        ----------
        event : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        # confirm close with user
        dlg = wx.MessageDialog(self.main, 
            "Do you really want to close this application?",
            "Confirm Exit", wx.OK|wx.CANCEL|wx.ICON_QUESTION)
        result = dlg.ShowModal()
        dlg.Destroy()
        
        if result == wx.ID_OK:
            # save the parameters for use next time
            self.saveParams(self.parameters)
            
            # stop the timers
            self.main.mainTimer.Stop()
            self.main.holdTimer.Stop()
            
            # close the connection to the motor controller
            try:
                self.arduino.close()
                print('Connection to motor controller closed.')
            except:
                pass
             
            # close the wx window
            self.main.Destroy()
            
            
            
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)