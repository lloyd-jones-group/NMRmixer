# NMRmixer

Control software for NMR mixing device for automated mixing of heterogeneous solid-liquid reactions, as described in Y. Gao, A.M.R. Hall, N.A. Fohn, E.J. King, L.A.L. Mitchell, G.A. Steedman and G.C. Lloyd-Jones, _Eur. J. Org. Chem._, **2024** (DOI: [10.1002/ejoc.202400095](https://dx.doi.org/10.1002/ejoc.202400095))

Email: [Prof. Guy Lloyd-Jones](mailto:guy.lloyd-jones@ed.ac.uk)\
Website: [Lloyd-Jones research group](https://www.lloyd-jones.chem.ed.ac.uk/)

## Installation ##
### Requirements ###
- Arduino Due
- [AccelStepper library](https://www.airspayce.com/mikem/arduino/AccelStepper/)
- Python 3.9 (optional*)
- wxPython 4.2.0 (optional*)
- pySerial 3.5 (optional*)

*Python execution only. Not required if using '.exe' file to start user interface.

Click on the blue 'Code' button above and select 'Download source code: zip'. Unzip the folder before running the programme.

### Arduino ###
Load the 'linearActuatorControl.ino' script located in '/Arduino/linearActuatorControl' onto the Arduino Due microcontroller board using the programming port. 

Note: An alternative script located in '/Arduino/linearActuatorControl_uno' is provided for compatibility with Arduino Uno microcontrollers, however the maximum speed of the stepper motor will be limited due to the lower clock speed of the Uno processor.

### User interface ###
Double clicking to run the 'NMR Mixing Controller.exe' file will start the user interface automatically (Note: Windows x64 only). This option does not require Python to be installed.

Alternatively, the user interface can be started directly using Python by executing the 'linearActuatorGUI.py' script located in '/linearActuatorControl'.

1. Communication to the Arduino microcontroller is initiated by clicking the 'Connect to System' button after connecting the Arduino to a USB port and powering on the motor controller. 
2. After connecting to the motor controller, the motor drive should be initialised using the 'Calibrate motor drive' function before use to determine the starting position of the motor. 
3. Fine tuning of the position can be adjusted from the 'Advanced/Move/Move to position' menu item. The 'Set home position' button sets the current position of the plunger as the lowest point of the plunger stroke (bottom of NMR tube).
4. Operating mode can be changed using the 'Advanced/Hold settings' menu item. Standard operation using the NMR spectrometer to control when to stop the motor requires the 'Hardware triggered' mode.
5. After starting, mixing will continue until either manually stopped or until a 'Hold' signal is received from the spectrometer (Hardware triggered mode). Upon receiving a 'Hold' signal, the motor will lift the plunger to the top of the NMR tube and wait until the 'Hold' signal is cleared. Similar behaviour occurs in the software triggered mode, with the difference that no external trigger signal is required.
6. Mixing speed and plunger stroke distance can be adjusted using the input boxes at any time.
7. Communication and motor parameters are saved in the 'setup.json' file on exit and are reloaded automatically when the software is started. 




